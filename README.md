# TuxGotHurt

An asynchronous API wrapper for a Roblox exploit called SirHurt for Linux & Windows

## Why?

+ Despite the fact that I hate SirHurt because the owner (IcePools) was being a pedophilia, the exploit runs in Wine too 🥴 (and its the only exploit I know that works in Wine)

+ When I found another free exploit that supports Wine, this project will be archived (Currently Krnl crashes).

## Current state

+ This API wrapper is in early phase, do not expect things to work well.

### Working features

+ Download exploit DLL

+ Download SirHurt MD5 hashed current version

### Unimplemented

+ Native Windows backend to contact with Roblox

+ Inject exploit to Roblox

+ Write MD5 hashed current version to Wine registry

+ Execute scripts

+ The script hub (Won't be implemented soon)

+ Bootstrapper class act like a real SirHurt bootstrapper
