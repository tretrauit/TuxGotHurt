import aiohttp
import hashlib
import platform
from asyncio import subprocess
from aiopath import AsyncPath

"""
Asynchronous bootstrapper wrapper for SirHurt, currently it does not support downloading official SirHurt GUI.
Largely based on my old SirHurtAPI C# code.
"""
class Bootstrapper():
    def __init__(self):
        # Don't hardcode URL in every functions.
        self.exploit_url = "https://sirhurt.net/asshurt/update/v4/fetch_version.php"
        self.exploit_version_url = "https://sirhurt.net/asshurt/update/v4/fetch_sirhurt_version.php"

    async def _get_exploit_url(self):
        """
        Get the current DLL download URL.
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(self.exploit_url) as rsp:
                if rsp.status != 200:
                    rsp.raise_for_status()
                # Get current exploit download URL.
                return await rsp.text()

    async def _get_current_version_md5(self):
        """
        Get the current SirHurt version.
        Return the MD5 hash of the current SirHurt version.
        """
        async with aiohttp.ClientSession() as session:
            async with session.get(self.exploit_version_url) as rsp:
                if rsp.status != 200:
                    rsp.raise_for_status()
                # Get current exploit version
                return hashlib.md5((await rsp.text()).encode('utf-8')).hexdigest()

    async def _download_exploit(self, save_location: str = "./"):
        """
        Download the current SirHurt.dll
        """
        exploit_current_url = await self._get_exploit_url()
        save_location_path = AsyncPath(save_location).joinpath("SirHurt.dll")
        async with aiohttp.ClientSession() as session:
            async with session.get(exploit_current_url) as rsp:
                if rsp.status != 200:
                    rsp.raise_for_status()
                async with save_location_path.open("wb") as f:
                    await f.write(await rsp.read())

    async def _write_version_to_registry(self, version: str):
        """
        Write version to the registry, usually SirHurt version hashed as MD5.
        @param version: The version to write to the registry.
        """
        # Is WINE Roblox working in macOS?
        if platform.system() == "Linux" or platform.system() == "Darwin":
            # Use grapejuice wine.
            await subprocess.create_subprocess_exec("wine", ["./TuxGotHurt-Windows.exe", "--write-sirhurt-version", version])
        elif platform.system() == "Windows":
            # Execute directly because we're on Windows.
            await subprocess.create_subprocess_exec("./TuxGotHurt-Windows.exe", ["--write-sirhurt-version", version])
        else:
            raise RuntimeError("Unsupported platform.")

    async def _write_current_version_to_registry(self):
        await self._write_version_to_registry(await self._get_current_version_md5())

    async def download(self, download_exploit_dll: bool = True, write_current_md5 = True, download_location: str = "./"):
        """
        Download the current SirHurt.dll
        @param download_location: The location to save the DLL.
        """
        if download_exploit_dll:
            await self._download_exploit(download_location)
        if write_current_md5:
            await self._write_current_version_to_registry()
        #raise NotImplementedError("Not implemented yet.")