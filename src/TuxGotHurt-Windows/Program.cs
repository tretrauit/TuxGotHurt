﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;

namespace TuxGotHurt_Windows
{
    class Program
    {
        static int Main(string[] args)
        {
            // Create a root command with some options
            var rootCommand = new RootCommand("A Windows <-> Python bridge to communicate with the exploit for TuxGotHurt")
            {
                new Option<string>(
                    "--write-sirhurt-version",
                    "Write SirHurt version to the system registry."),
                new Option<int>(
                    "--int-option",
                    getDefaultValue: () => 42,
                    description: "An option whose argument is parsed as an int"),
                new Option<bool>(
                    "--bool-option",
                    "An option whose argument is parsed as a bool")
            };

            // Note that the parameters of the handler method are matched according to the names of the options
            rootCommand.Handler = CommandHandler.Create((string stringOption, int intOption, bool boolOption) =>
            {
                Console.WriteLine(stringOption);
                Console.WriteLine($"The value for --int-option is: {intOption}");
                Console.WriteLine($"The value for --bool-option is: {boolOption}");
                Console.WriteLine($"The value for --write-sirhurt-version: {stringOption}");
            });

            // Parse the incoming args and invoke the handler
            return rootCommand.InvokeAsync(args).Result;
        }
    }
}
